---
author: Jonas Reitz
---

# IntelliJ IDEA 

- [IntelliJ IDEA](#intellij-idea)
  - [Arbeiten mit Gradle in IntelliJ](#arbeiten-mit-gradle-in-intellij)
  - [Arbeiten mit Git](#arbeiten-mit-git)
    - [Commit & Push](#commit--push)
    - [Merge](#merge)
    - [Bestehendes Projekt von Git importieren](#bestehendes-projekt-von-git-importieren)

## Arbeiten mit Gradle in IntelliJ

Nachdem Sie ein Gradle-Projekt über die Konsole angelegt (für [Java](GradleJava.md) bzw. [Kotlin](GradleKotlin.md)) oder eines über Git geclont haben, wollen Sie natürlich anfangen zu programmieren. Bereits beim ersten Start von IntelliJ fragt Sie die Entwicklungsumgebung, welchen `Workspace` Sie nutzen wollen. Sollten Sie bereits ein Projekt offen haben, können Sie dieses über `File -> Close Project` schließen.

Wenn Sie vorher mit anderen Entwicklungsumgebungen gearbeitet haben, kann dies leicht verwirren. In IntelliJ ist mit dem Ausdruck "Workspace" gleichzeitig das Projekt gemeint, an dem man arbeiten möchte. Wenn Sie ein bestehendes Gradle-Projekt importieren möchten, wählen Sie `Import Project`.    

![Start](IntelliJIDEA/start.png)

Nachdem Sie einen Ordner ausgewählt haben, möchte IntelliJ wissen, was für ein Projekt es ist. Dort wählen Sie dann das Gradle-Model für das Projekt aus.   

![](IntelliJIDEA/asGradle.png)

Haben Sie das Projekt geöffnet, legt IntelliJ automatisch einen `.idea`-Ordner im Projektverzeichnis an. Dieser Ordner enthält Informationen über den Workspace, die nur IntelliJ für sich benötigt. Sie sollten `.idea` unbedingt von der Versionsverwaltung durch Git ausschließen, indem Sie den Ordner in der `.gitignore`-Datei hinzufügen.

Wenn Sie Ihr geöffnetes Projekt in IntelliJ übersetzen und ausführen wollen, gehen Sie in den linken Tab `Gradle` und wählen dort unter `Tasks -> application ` den `run` task aus.

![](IntelliJIDEA/gradleRun.PNG)

Und schon sehen Sie unten in der Konsole die Ausgabe `Hello World!`. Sobald Sie das Projekt einmal über den `run`-Task gestartet haben, können Sie es erneut über den `Pfeil`-Button starten. Außerdem hat Ihnen IntelliJ ein `Run Configuration` angelegt hat -- im Beispiel `tictactoe [run]`.

![](IntelliJIDEA/normalStartConfig.PNG)    

Sollten Sie die `build.gradle` bearbeitet haben, vergessen Sie nicht, den `refresh`-Button (siehe Bild) zu benutzen, um die neuen Abhängigkeiten herunterzuladen, sodass Sie auch in IntelliJ richtig benutzt werden. Sonst werden Fehler im Code anzeigt.

![](IntelliJIDEA/gradleRefresh.png)    

## Arbeiten mit Git

### Commit & Push

Sollten Sie in einer Gruppe oder mit verschiedenen Rechnern an dem gleichen Projekt arbeiten, empfiehlt sich die Arbeit mit mit der Versionskontolle Git.    

Sollten Sie ein bereits bestehendes Projekt in IntelliJ mit Git verknüpft haben, solltet Sie den Tab `Version Control` am unteren Bildschirmrand finden. Wenn Sie Änderungen an dem Projekt als `Commit` speichern möchten, können Sie das über den unten gezeigten, grünen Haken tun (oder über `Strg+K`).

![1](IntelliJIDEA/git1.PNG)

Dort können Sie dann die betroffenen Dateien auswählen und eine `Commit Message` eingeben, die den Inhalt des Updates bündig zusammenfasst. Die Änderung sollte mit einer prägnanten und aussagekräftigen Commit-Nachrichten versehen sein.

![1](IntelliJIDEA/git2.PNG)

Über die Tastenkombination `Strg + Shift + K` können Sie dann alle Commits `pushen`, welche Ihre Änderungen auf einen Git-Server wie GitHub oder git.thm.de überträgt.

![1](IntelliJIDEA/git3.PNG)

### Merge

Wenn Sie eine Änderungen von dem Online-Repository `mergen` wollen, können Sie das über die Tastenkombination `Strg + T` oder über den blauen Pfeil im Git-Menü in der oberen Rechten Ecke machen. Beim `mergen` wird das lokale Repository (das momentane Projekt) mit dem online Repository verglichen und es werden Änderungen ausgetauscht und aktualisiert. So sind Sie auf dem aktuellsten Stand ohne befürchten zu müssen, das Ihre neu erstellten lokalen Dateien verschwinden.

### Bestehendes Projekt von Git importieren

Wollen Sie ein bestehendes Projekt `clonen` können Sie das im Start-Bildschirm von IntelliJ über `Get from Version Control` und der passenden Repository-Url tun.
