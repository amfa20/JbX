# Abgabe der Gruppenarbeit

Sie haben in einer Gruppe ein Programmierprojekt mit Javalin umgesetzt. Code und Dokumentation sollen so gestaltet sein, dass andere Studierende sich Anregungen, Ideen und vor allem Beispiele der Umsetzung holen können. 

Besonderen Wert legen wir auf die Dokumentation. Die Dokumentation Ihres Projekt und des Codes ist von Studierenden für Studierende gedacht. Sie hat das Ziel und den Zweck, Ihren Kommilitonen eine echte Hilfe zu sein. Sie brauchen das Triviale nicht zu erklären. Aber Sie haben vermutlich ein sehr gutes Gefühl dafür, was Sie gerne zu Beginn Ihres Projektes gewusst und liebend gerne nachgelesen hätten. Nehmen Sie das als Richtschnur.

## Zur Dokumentation

Jedes `README.md` beginnt bitte wie folgt:

~~~
# Projekt: <Names des Projekts> (Fr/1, Kr)

> Beschreiben Sie in 150 bis max. 200 Wörtern knapp, was man mit Ihrer Anwendung machen kann bzw. was ihr Zweck ist. 

![Screenshot](Screenshot.png)

Keywords: Websockets, Bootstrap, responsive Design, Server-Sent Events (SSE), MongoDB

Projektbeteiligte:

* Aaron Bartsch
* Leonie Kramer
* Klara Langenberg
* Oznur Zuric

<Inhaltsverzeichnis>
~~~

* Der Projektname soll eine Idee vermitteln, worum es in Ihrer Anwendung geht.
* Ergänzen Sie hinter dem Projektnamen den von Ihnen besuchten Praktikumstermin wie beispielhaft als Kürzel gezeigt: `(<Tag>/<Block>, (Hb|Kr))`.
* Belassen Sie in der Kurzbeschreibung am Zeilenanfang das führende Größerzeichen "`>`". 
* Fügen Sie einen Screenshot Ihrer Anwendung in Aktion ein.
* Listen Sie hinter "`Keywords:`" Stichworte auf, die das Projekt _technisch_(!) in Bezug auf Javalin, eingesetzte Bibliotheken etc. charakterisieren. So können andere schnell orten, ob Ihr Projekt für sie von Interesse sein könnte, wenn man auf der Suche nach technischen Umsetzungsbeispielen ist.
* Listen Sie in alphabetischer Reihenfolge die Namen der Projektbeteiligten auf. Bitte geben Sie _keine_ Matrikelnummern an.
* Es gibt Editoren, wie Visual Studio Code, für die Sie eine Erweiterung herunterladen können, damit Sie in einem Markdown-Dokument ein Inhaltsverzeichnis (_Table of Content_) einfügen und automatisch aktualisieren lassen können. Wenn Sie das hinbekommen, wäre das super!

Nach dieser Einleitung sind Sie frei in der weiteren Gestaltung der Dokumentation.

Was zeichnet eine gute Dokumentation aus? Lesbarkeit, Verständlichkeit, eine klare Gliederung und Struktur, Korrektheit der Sachinformationen, eine Abwesenheit von Schreib- und Grammatikfehlern. Und: Die Dokumentation soll aus dem Blickwinkel des Lesers geschrieben sein. Hilfreich, erklärend und dennoch auf den Punkt gebracht. Beispiele sind immer gut!

> Die Dokumentation ist verpflichtend in Markdown (gerne GitHub/GitLab-Flavour) zu schreiben.

Sie dürfen selbstverständlich auch Ihren Programmcode mit Kommentaren und Anmerkungen versehen. Aber übertreiben Sie es nicht. Zuviel Dokumentation im Code schadet dem Überblick. Besser ist es oft, entscheidende Codeabschnitte separat in einer Dokumentationsdatei (z.B. im `README.md`) zu besprechen und vorzustellen. Und auch ein Überblick über den Aufbau Ihres Codes hilft der Orientierung.

> Bitte erstellen Sie nur eine Datei namens `README.md`, in der sich die gesamte Dokumentation befindet. Teilen Sie die Dokumentation bitte nicht auf mehrere Dateien auf. Bilder und sonstige Dateien, die Sie einbinden, können Sie gerne auf der gleichen Verzeichnisebene wie das `README.md` belassen.

## Abgabe

Ihr Team liefert über eine für die Abgabe verantwortliche Person die Programmdateien inklusive Dokumentation ab. Wie gehabt ist das Projekt im zip-Format in Moodle hochzuladen -- beachten Sie die dort angegebene Deadline; Sie können bis zur Deadline Ihre Abgabe beliebig oft aktualisieren. Die Verzeichnisstruktur orientiert sich an den für Gradle üblichen Konventionen. Abzuliefern ist ein auf das Nötigste bereinigte Verzeichnis aus Quellcode, Dokumentation und eventuellen Tests. Die Ausführung der Anwendung muss mit `gradle run` zu starten sein.

Zur Erinnerung: Wir werden Ihre Code-Beispiele den anderen PiS-Studierenden in Moodle zur Verfügung stellen. Da in Ausnahmefällen in Ihrem Code teils Passwörter oder Zugangskennungen (z.B. bei Nutzung externer APIs) stehen werden, geben Sie in dem Fall bitte zwei gezippte Varianten ab: eine, die wir problemlos ausprobieren und abnehmen können, und eine ansonsten praktisch identische Fassung, die jedoch ohne Kennwörter oder Zugangscodes. Machen Sie die unterschiedlichen zip-Files durch Namensgebung eindeutig für uns kenntlich. 
