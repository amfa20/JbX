import io.javalin.Javalin;
import io.javalin.core.util.FileUtil;
import java.util.HashMap;
import java.util.Map;

public class Main {

    /* Die Map `reservations` wird mit zwei Key/Value-Paaren initialisiert.
       Das sind die Daten, die der Server verwaltet. Da die Daten nicht
       in einer Textdatei oder in einer Datenbank gespeichert werden,
       gehen sie mit dem Beenden des Servers verloren.
    */
    private static Map<String, String> reservations = new HashMap<>() {{
        put("saturday", "No reservation");
        put("sunday", "No reservation");
    }};

    public static void main(String[] args) {

        /* Webserver aufsetzen: https://javalin.io/documentation#server-setup
           - mit direktem Zugriff auf Dateien in `/resources/public`,
             wobei `index.html` bei `http://{host}:{port}/` ausgeliefert wird
           - mit Port 7070 als Eingang für HTTP-Requests, wobei der Webserver
             Anfragen synchron verarbeitet, d.h. während der Verarbeitung
             keine weiteren Anfragen gleichzeitig bearbeiten kann.
        */
        Javalin app = Javalin.create()    
            .enableStaticFiles("/public")
            .start(7070);

        /* Für `app` werden mehrere Reaktionen auf HTTP-Anfragen eingerichtet.

           `.post()` und `.get()` sind sogenannte _Endpoint Handler_ (siehe
           https://javalin.io/documentation#endpoint-handlers) für die
           HTTP-Anfragemethoden `POST` und `GET`.

           - `POST` dient zur Übermittlung meist größerer Datenmengen
           - `GET` soll laut Standard nur Daten abrufen, wobei Argumente in
              der URI mitgeliefert werden können

           Die Endpoint Handler können einen Pfad enthalten.
           Das `Context`-Objekt (https://javalin.io/documentation#context)
           enthält alles, um eine HTTP-Anfrage zu behandeln (_request methods_)
           und um eine HTTP-Antwort aufzusetzen (_response methods_).

           Hier wird per Lambda-Ausdruck der Umgang mit der HTTP-Anfrage
           programmiert.

        */
        /* Die Reservierung mit den Daten aus Form-Parametern werden in der
           Map abgelegt mit der Tagesangabe (ermittelt über den Parameter
           `day`) als Schlüssel und der Uhrzeitangabe (ermittelt über `time`)
           als Wert.

        */ 
        app.post("/make-reservation", ctx -> {
            reservations.put(ctx.formParam("day"), ctx.formParam("time"));
            ctx.html("Your reservation has been saved");
        });

        /* Die für einen Tag hinterlegte Uhrzeit wird abgefragt.
        */ 
        app.get("/check-reservation", ctx -> {
            ctx.html(reservations.get(ctx.queryParam("day")));
        });

        /* Die mit der POST-Methode übertragenen Dateien werden im
           Projekt-Ordner `/upload` abgelegt. 
        */
        app.post("/upload-example", ctx -> {
            ctx.uploadedFiles("files").forEach(file -> {
                FileUtil.streamToFile(file.getContent(), "upload/" + file.getName());
                ctx.html("Upload successful");
            });
        });
    }
}


