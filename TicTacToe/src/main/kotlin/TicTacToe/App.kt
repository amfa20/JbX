package TicTacToe

import io.javalin.Javalin
import io.javalin.http.Context


class App {
    private val count = 0
    init {
        val game = T3()

        val app = Javalin.create{config ->
            config.addStaticFiles("/public")
        }.start(7070)

        app.get("/move") { ctx: Context ->
            val input = ctx.queryParam("pos")!!.toInt()
            if (game.isValidMove(input)) game.move(input)
            ctx.result(game.toString())
        }
        app.get("/newgame") { ctx: Context ->
            game.undoAll()
            ctx.result(game.toString())
        }
        app.get("/undo") { ctx: Context ->
            game.undo()
            ctx.result(game.toString())
        }
        app.get("/rows") { ctx: Context ->
            ctx.result(game.toString())
        }

    }

}

fun main(args: Array<String>) {
    App()
}
